const readMore1 = $('.read-more-button-1')
const readMore2 = $('.read-more-button-2')
const readMore3 = $('.read-more-button-3')
const article1 = $('.read-more-content-1')
const article2 = $('.read-more-content-2')
const article3 = $('.read-more-content-3')

$(document).ready(function () {
    article1.hide();
    article2.hide();
    article3.hide();

    // search bar and scroll to view

    $('#search-btn').click(function (e) {
        e.preventDefault();
        let input = $("#search-input").val()
        if (input != "") {
            $("p:contains('" + input + "')").css("background-color", "yellow").prepend(`<span class="scroll-to-me"></span>`).focus();
        }
    });

    // Carousel - Random starter image
    const images = ['hero-image.jpg', 'hero-image-4.jpg', 'hero-image-5.jpg', 'hero-image-6.png', 'hero-image-7.jpg', 'hero-image-8.jpg'];
    $('<img class="d-block w-100 car-img" alt="coffee hero image" src="../src/img/' + images[Math.floor(Math.random() * images.length)] + '">').appendTo('.active');

});

// quick hide and show read more functions

readMore1.on('click', function () {
    if (article1.css('display') === "none") {
        readMore1.html("Read Less");
        article1.show();

    } else {
        readMore1.html("Read More");
        article1.hide();
    }
});

readMore2.on('click', function () {
    if (article2.css('display') === "none") {
        readMore2.html("Read Less");
        article2.show();

    } else {
        readMore2.html("Read More");
        article2.hide();
    }
});

readMore3.on('click', function () {
    if (article3.css('display') === "none") {
        readMore3.html("Read Less");
        article3.show();

    } else {
        readMore3.html("Read More");
        article3.hide();
    }
});